<html>
<head>
<title>Princess Pi's Parrot!</title>
<style type="text/css">
body {
    font-family: Georgia,Palatino,serif;
    background-color: #FFDDDD;
}

h1.title {
    color: purple;
	font-size: 3em;
}

img { border: 0; }

input[type=text],textarea { 
    border: 2px solid purple;
    background-color: #FFEEEE;
}

input[type=text]:focus,textarea:focus {
    border: 2px solid pink;
}

input[type=text],textarea { 
    border: 2px solid purple;
    background-color: #FFEEEE;
}

input[type=text]:focus,textarea:focus {
    border: 2px solid pink;
}

select {
    border: 2px solid purple;
    background-color: #FFEEEE;
}

select:focus {
    border: 2px solid pink;
}

input[type=button],input[type=file],input[type=submit] {
    border: 2px solid purple;
    background: #FFEEEE;
}

input[type=button]:hover,input[type=file]:hover,input[type=submit]:hover {
    background: #FFAAAA;
}
</style>
</head>
<body>
<h1 class="title">Princess Pi's Parrot!</h1>
<p>Say something and listen to Princess Pi's pet parrot echo it back to you!</p>
<form action="" method="get">
GET:<br>
<textarea name="get"></textarea><br>
<input type="submit" value="Send as GET">
</form>
<br><br><br>
<form action="" method="post">
POST:<br>
<textarea name="post"></textarea><br>
<input type="submit" value="Send as POST">
</form>
<br><br>
<div>
<?php
echo $_GET['get'];
echo $_POST['post'];
?>
</div>
<a href="images/princesspilarge.png"><img src="images/princesspismall.png"></a>
</body>
</html>