<html>
<head>
<title>PadCipher - Generate Pad</title>
<link rel="stylesheet" type="text/css" href="/Access/style.css">
</head>
<body>
<div id="content">
<h1>Generate a Pad</h1>
<p>In order to use the PadCypher Encryption System, you first must generate a pad of equal or greater length to the message(s) You will be sending.</p>

<p>We will never use a pad of the exact size of your message, because that gives one more piece of information to an attacker that may be used against you.</p>

<p>Begin by selecting the maximum length of the message(s) you intend to send, and then download a random pad file.</p>

<?php include('menu.php'); ?>
<form>
<fieldset>

<legend>Select a Pad Size</legend>

<input type="radio" name="length" value="10" id="ten">

  <label for="ten">Ten Characters</label><br>

<input type="radio" name="length" checked="checked" value="100" id="hundred">

  <label for="hundred">One Hundred Characters</label><br>

<input type="radio" name="length" value="1000" id="thousand">

  <label for="thousand">One Thousand Characters</label><br>

<input type="radio" name="length" value="10000" id="tenthousand">

  <label for="tenthousand">Ten Thousand Characters</label><br>

<input type="button" onclick="generatePad();" value="Generate Pad">

<p id="dtext"><a id="dlink" href="">Download Pad</a></p>
</form>

<script src="downloader.js"></script>

<script>
function generatePad() {

var pad = '';

for(i=0;i<document.forms[0].length.value;i++) {
    pad += Math.floor((Math.random() * 32768)+1) + '-';
}

pad = pad.slice(0,-1);
download(pad,'EncryptedPad.pad','text/plain');
}
</script>
</body>
</html>