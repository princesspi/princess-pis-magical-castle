<?php
if($_GET['header']) {
    header($_GET['header']);
}
?>
<html>
<head>
<title>Princess Pi's Header Tool!</title>
<style type="text/css">
body {
    font-family: Georgia,Palatino,serif;
    background-color: #FFDDDD;
}

h1.title {
    color: purple;
	font-size: 3em;
}

img { border: 0; }

input[type=text],textarea { 
    border: 2px solid purple;
    background-color: #FFEEEE;
}

input[type=text]:focus,textarea:focus {
    border: 2px solid pink;
}

input[type=text],textarea { 
    border: 2px solid purple;
    background-color: #FFEEEE;
}

input[type=text]:focus,textarea:focus {
    border: 2px solid pink;
}

select {
    border: 2px solid purple;
    background-color: #FFEEEE;
}

select:focus {
    border: 2px solid pink;
}

input[type=button],input[type=file],input[type=submit] {
    border: 2px solid purple;
    background: #FFEEEE;
}

input[type=button]:hover,input[type=file]:hover,input[type=submit]:hover {
    background: #FFAAAA;
}
</style>
</head>
<body>
<h1 class="title">Princess Pi's Personal Header Tool!</h1>
<p>Princess Pi wants you to get aHEAD in life!</p>
<form action="" method="get">
<input type="text" name="header"> <input type="submit" value="Send Header">
</form>
<a href="images/princesspilarge.png"><img src="images/princesspismall.png"></a>
</body>
</html>