<?php
$files = "conf/smaert.com.txt";
$lines = file($files);
$out = "";

foreach($lines as $uri) {
	$out.="<h3><a href='$uri'>$uri</a></h3><iframe src='$uri'></iframe>";
}

?>
<html>
<head>
<title>Princess Pi's Multiple File View</title>
<link rel="stylesheet" type="text/css" href="style.css">
<style>
	body { background-image('images/princesspi.png');
				 background-position:scroll;}
	iframe { width: 80%;
					 height: 55px; }
</style>
</head>
<body>


<div id="content">
<h1 class="title">Princess Pi's Multiple File Vew!</h1>
<p>Got a whole stack of URIs to manually check? Princess Pi is always here to help!</p>
<?=$out?>
</div>
</body>
</html>